<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Album;
use AppBundle\Request\AlbumRequest;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * Get list of Albums
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Get list of albums",
     *  output="JSON",
     *  section = "Gallery"
     * )
     *
     * @Route("/", name="homepage")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        // 1. get Albums
        $albums = $em->getRepository('AppBundle:Album')->getAll();

        // 2. return response
        if ($request->isXmlHttpRequest()) {
            $json = $this->container->get('app.serializer')->serialize($albums, 'json');

            return new Response($json);
        }

        return $this->render('default/album_list.html.twig', array(
            'albums' => $albums
        ));
    }

    /**
     * Get Album with first Images page
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Get exact album and",
     *  output="JSON",
     *  parameters={
     *      {"name"="id", "dataType"="integer", "required"=true, "format" = "\d+", "description" = "Album id"}
     *  },
     *  section = "Gallery"
     * )
     *
     * @Route("/album/{id}", name="album")
     */
    public function getAlbumAction(Request $request, $id)
    {
        return $this->getAlbumWithPicsPageAction($request, $id, $page = 1);
    }

    /**
     * @param $id
     * @param $page
     * @return AlbumRequest
     * @throws \Exception
     */

    private function processRequest($id, $page)
    {
        /** @var Album $album */
        $em = $this->getDoctrine()->getManager();

        // 1. Check if album exists
        $album = $em->getRepository('AppBundle:Album')->find($id);

        if (!$album) {
            throw new \Exception('Resource not found', 404);
        }

        /** @var Paginator $album_images */
        $images_paginator = $this->container->get('app.album_images_service')->getAlbumImages($album->getId(), $page);

        return new AlbumRequest($album, $images_paginator);
    }

    /**
     * Get Album with given Images page
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Get exact album",
     *  output="JSON",
     *  parameters={
     *      {"name"="id", "dataType"="integer", "required"=true, "format" = "\d+", "description" = "Album id"},
     *      {"name"="page", "dataType"="integer", "required"=true, "format" = "\d+", "description" = "Page: 2,3,4... "}
     *  },
     *  section = "Gallery"
     * )
     *
     * @Route("/album/{id}/page/{page}", name="album_view")
     * @Method("GET")
     */
    public function getAlbumWithPicsPageAction(Request $request, $id, $page = 1)
    {
        $res = $this->processRequest($id, $page);
        $album = $res->getAlbum();
        $images_paginator = $res->getPaginator();
        $images_paginator->setUsedRoute('album_view');

        // 2. if AJAX request, return JSON response
        if ($request->isXmlHttpRequest()) {
            $json = $this->container->get('app.serializer')->serialize($images_paginator->getItems(), 'json');

            return new Response($json);
        }

        return $this->render('default/album_view.html.twig', array(
            'album' => $album,
            'images_paginator' => $images_paginator,
        ));
    }

    /**
     * Special request to render pager by marionette.js
     * only AJAX requests are allowed
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Get rendered album pager",
     *  output="JSON",
     *  parameters={
     *      {"name"="id", "dataType"="integer", "required"=true, "format" = "\d+", "description" = "Album id"},
     *      {"name"="page", "dataType"="integer", "required"=true, "format" = "\d+", "description" = "Page: 2,3,4... "}
     *  },
     *  section = "Gallery"
     * )
     *
     * @Route("/album_pager/{id}/page/{page}", name="album_pager")
     * @Method("GET")
     */
    public function getAlbumPagerAction(Request $request, $id, $page)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new \Exception('Bad request', 401);
        }

        $res = $this->processRequest($id, $page);

        /** @var Paginator $album_images */
        $images_paginator = $res->getPaginator();

        $json = $this->container->get('app.serializer')->serialize(
            $this->pagerToArray($id, $page, $images_paginator)
            , 'json');

        return new Response($json);
    }

    /**
     * Convert Paginator object to array, which contains link attributes
     *
     * @param $album_id
     * @param $page
     * @param PaginationInterface $images_paginator
     * @return array
     */
    private function pagerToArray($album_id, $page, PaginationInterface $images_paginator)
    {
        $pager = [];
        $pages = ceil($images_paginator->getTotalItemCount() / $images_paginator->getItemNumberPerPage());
        for ($p = 1; $p <= $pages; $p++) {
            $pager[] = [
                'page' => $p,
                'album_id' => $album_id,
                'css_class' => $p == $page ? 'active' : '',
            ];
        }

        return count($pager) > 1 ? $pager : 0;
    }

}
