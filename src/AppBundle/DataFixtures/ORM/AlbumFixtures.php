<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Album;
use AppBundle\Entity\Image;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Load albums
 */
class AlbumFixtures extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 1;
    }

    public function load(ObjectManager $manager)
    {

        $album_pics_cnt = [5, rand(20, 40), rand(20, 40), rand(20, 40), rand(20, 40)];

        for ($i = 1; $i <= 5; $i++) {
            $album = new Album();
            $album->setName("New album #{$i}");
            $manager->persist($album);

            for ($j = 1; $j <= $album_pics_cnt[$i - 1]; $j++) {
                $image = new Image();
                $image->setAlbum($album);
                $image->setTitle('image title #'.$j);
                $image->setPic('http://placehold.it/800x600');

                $manager->persist($image);
            }

            $manager->flush();
        }
    }

}