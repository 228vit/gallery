<?php
/**
 * Created by PhpStorm.
 * User: vit
 * Date: 4/11/16
 * Time: 12:51 AM
 */

namespace AppBundle\Service;


interface SerializerServiceInterface {
    public function serialize($object, $format);
}