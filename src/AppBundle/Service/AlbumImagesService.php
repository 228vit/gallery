<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityRepository;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;

class AlbumImagesService
{
    private $repository;
    private $paginator;

    public function __construct(EntityRepository $repository, Paginator $paginator)
    {
        $this->repository = $repository;
        $this->paginator = $paginator;
    }

    /**
     * Get page of album images
     *
     * @param $album_id
     * @param int $page
     * @return PaginationInterface
     */
    public function getAlbumImages($album_id, $page = 1)
    {
        // 1. get query
        $query = $this->repository->getAlbumImagesQuery($album_id);

        // 2. get page
        $pagination = $this->paginator->paginate(
            $query,
            $page,
            10 /*limit per page*/
        );

        return $pagination;
    }

}