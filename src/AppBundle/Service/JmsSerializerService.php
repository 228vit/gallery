<?php
/**
 * Created by PhpStorm.
 * User: vit
 * Date: 4/12/16
 * Time: 3:09 PM
 */

namespace AppBundle\Service;


use JMS\Serializer\SerializerInterface;

class JmsSerializerService implements SerializerServiceInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }


    /**
     * @param $object
     * @param string $format
     * @return mixed
     */
    public function serialize($object, $format = 'json')
    {
        return $this->serializer->serialize($object, $format);
    }

}