<?php

namespace AppBundle\Request;

use AppBundle\Entity\Album;
use Knp\Component\Pager\Pagination\PaginationInterface;

class AlbumRequest
{
    /** @var Album */
    private $album;

    /** @var PaginationInterface */
    private $paginator;

    function __construct(Album $album, PaginationInterface $paginator)
    {
        $this->album = $album;
        $this->paginator = $paginator;
    }

    /**
     * @return Album
     */
    public function getAlbum()
    {
        return $this->album;
    }

    /**
     * @return PaginationInterface
     */
    public function getPaginator()
    {
        return $this->paginator;
    }


}