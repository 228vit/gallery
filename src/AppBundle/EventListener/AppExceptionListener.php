<?php
namespace AppBundle\EventListener;

use Assetic\Exception\Exception;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class AppExceptionListener
{

    private $logger;

    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        /** @var \Exception $e */
        $e = $event->getException();

        $this->logger->error($e->getMessage(), [
            'line' => $e->getLine(),
            'err_code' => $e->getCode(),
            'trace' => $e->getTraceAsString(),
        ]);


        $response = new JsonResponse([
            'status' => 'fail',
            'error_code' => $e->getCode(),
            'error_description' => $e->getMessage(),
//            'trace' => $e->getTraceAsString(),
        ], $e->getCode());

        $event->setResponse($response);
    }
}