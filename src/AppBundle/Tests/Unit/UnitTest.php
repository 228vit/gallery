<?php
namespace AppBundle\Tests\Unit;

use AppBundle\Entity\Album;
use AppBundle\Entity\Image;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class UnitTests extends \PHPUnit_Framework_TestCase
{
    private $container;

    public function setUp()
    {
        // TODO: remove
        $this->container = new ContainerBuilder();
    }

    public function tearDown()
    {
    }

    public function testRepositories()
    {
        // test Album repository
        $album_mock = $this->getMockBuilder('\AppBundle\Repository\AlbumRepository')
            ->disableOriginalConstructor()
            ->getMock();

        // fake Album
        $album = new Album();
        $album->setId(1);
        $album->setName('Album #1');

        // try to find Album
        $album_mock->expects($this->once())
            ->method('find')
            ->will($this->returnValue($album));

        $result = $album_mock->find(1);
        $this->assertTrue($result instanceof Album);

        // try to get all Albums
        $album_mock->expects($this->once())
            ->method('getAll')
            ->will($this->returnValue([0 => $album, 1 => $album, 2 => $album]));

        $result = $album_mock->getAll();
        $this->assertTrue(is_array($result));


        // mock page of Images
        $images = array();
        for ($i = 1; $i <= 10; $i++) {
            $image = new Image();
            $image->setAlbum($album);
            $image->setTitle('Image #' . $i);
            $image->setPic('http://placehold.it/200x200');

            $images[] = $image;
        }

        // test Images repository
        $image_mock = $this->getMockBuilder('\AppBundle\Repository\ImageRepository')
            ->disableOriginalConstructor()
            ->getMock();

        $image_mock->expects($this->once())
            ->method('getAlbumImagesQuery')
            ->will($this->returnValue($images));

        // try to get Album with Images
        $result = $image_mock->getAlbumImagesQuery(1);
        $this->assertTrue(is_array($result));

    }

    public function testSerializer()
    {
        echo __METHOD__ . "\n";

        $mock = $this->getMockBuilder('\AppBundle\Service\JmsSerializerService')
            ->disableOriginalConstructor()
            ->getMock();

        $data = ['foo' => 'bar'];
        $json = json_encode($data);

        $mock->expects($this->once())
            ->method('serialize')
            ->will($this->returnValue($json));

        $result = $mock->serialize($data, 'json');


        $this->assertEquals((string)$result, (string)$json);
    }

}