<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        echo __METHOD__."\n";

        $client = static::createClient();

        // 1. get Albums list
        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Albums', $crawler->filter('.container h1')->text());

        // 2. click on link and get first album
        $album_url = $crawler->filter('.container ul li a')->attr('href');

        $crawler = $client->request('GET', $album_url);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('View album', $crawler->filter('.container h1')->text());

        // AJAX requests!!!
        $client->request('GET', '/', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ));
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // get JSON
        $json = $client->getResponse()->getContent();
        // lets decode, and check if array
        $data = json_decode($json, true);
        $this->assertTrue(is_array($data));

        // 2.1 same request, but AJAX
        $client->request('GET', $album_url, array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ));
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // get JSON
        $json = $client->getResponse()->getContent();
        // lets decode, and check if array
        $data = json_decode($json, true);
        $this->assertTrue(is_array($data));


        // 2.2 fake pager AJAX request
        $client->request('GET', '/album_pager/2/page/1', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ));
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $json = $client->getResponse()->getContent();
        // lets decode, and check if array
        $data = json_decode($json, true);
        $this->assertTrue(is_array($data));

        // 2.3 fake pager, but not AJAX request
        $client->request('GET', '/album_pager/2/page/1', array(), array(), array());
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }

    public function test404()
    {
        echo __METHOD__ . "\n";
        $client = static::createClient();

        // test 404
        $client->request('GET', '/album/0');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());

    }
}
