var GalleryManager = new Marionette.Application();

GalleryManager.navigate = function (route, options) {
    options || (options = {});
    Backbone.history.navigate(route, options);
};

GalleryManager.getCurrentRoute = function () {
    return Backbone.history.fragment
};

/*
 * Regions
 */
GalleryManager.on("before:start", function () {
    var RegionContainer = Marionette.LayoutView.extend({
        el: "#app-container",
        regions: {
            main: "#main-region",
            pics: "#pics-region",
            pager: "#pager-region"
        }
    });
    GalleryManager.regions = new RegionContainer();
});

/*
 * Albums
 */
var AlbumModel = Backbone.Model.extend({
    defaults: {
        id: 1,
        name: "Album #1",
        url: "album/1"
    }
});

var AlbumsCollection = Backbone.Collection.extend({
    model: AlbumModel,
    url: 'http://127.0.0.1:8000/app_dev.php/'
});

// child must be defined before parent
GalleryManager.AlbumItemView = Marionette.ItemView.extend({
    tagName: "li",
    className: "albums-navi",
    template: "#album-list-item",
    events: {
        'click a': 'clickedButton'
    },

    clickedButton: function () {
        console.log('I clicked the button!');
        console.log(this.model.id);
        $('.albums-navi').removeClass('active');
        $('#album_' + this.model.id).parent().addClass('active');
    }
});

GalleryManager.AlbumsCollectionView = Marionette.CollectionView.extend({
    tagName: "ul",
    className: "nav nav-pills nav-stacked",
    childView: GalleryManager.AlbumItemView
});


/*
 * Images
 */
var ImageModel = Backbone.Model.extend({
    defaults: {
        id: 1,
        album_id: 1,
        title: "Album #1",
        thumb: "http://placehold.it/120x80",
        pic: "http://placehold.it/800x600",
    }
});

var ImagesCollection = Backbone.Collection.extend({
    model: ImageModel,
    url: 'http://127.0.0.1:8000/app_dev.php/album'
});

GalleryManager.ImageItemView = Marionette.ItemView.extend({
    tagName: "div",
    className: "col-xs-3 col-md-2",
    template: "#album-image-template"
});

GalleryManager.ImagesCollectionView = Marionette.CollectionView.extend({
    tagName: "ul",
    childView: GalleryManager.ImageItemView
});

/*
 * PagerLinks
 */
var PagerLinkModel = Backbone.Model.extend({
    defaults: {
        id: 1,
        page: 1,
        css_class: ''
    }
});

var PagerLinksCollection = Backbone.Collection.extend({
    model: PagerLinkModel,
    url: 'http://127.0.0.1:8000/app_dev.php/album_pager'
});

GalleryManager.PagerLinkItemView = Marionette.ItemView.extend({
    tagName: "li",
    template: "#pager-template"
});

GalleryManager.PagerLinksCollectionView = Marionette.CollectionView.extend({
    tagName: "ul",
    className: "pagination",
    childView: GalleryManager.PagerLinkItemView
});

/*
 * Application
 */
GalleryManager.module("GalleryApp", function (GalleryApp, GalleryManager, Backbone, Marionette, $, _) {

    GalleryApp.Router = Marionette.AppRouter.extend({
        appRoutes: {
            "album/:id": "showAlbum",
            "album/:id/page/:page": "showAlbumPage"
        }
    });

    GalleryManager.on("albums:list", function () {
        API.listAlbums();
    });

    var API = {
        listAlbums: function () {
            GalleryApp.List.Controller.listAlbums();
        },
        showAlbumPage: function (id, page) {
            GalleryApp.View.Controller.viewAlbum(id, page);
            // hmmm....
            $('#pics-header-region').html('<h1>View album: ' + id + '</h1>');
        },
        showAlbum: function (id) {
            this.showAlbumPage(id, 1);
        }
    };

    GalleryApp.on("start", function () {
        new GalleryApp.Router({
            controller: API
        });
    });
});

GalleryManager.module("GalleryApp.List", function (List, GalleryManager, Backbone, Marionette, $, _) {
    List.Controller = {
        listAlbums: function () {
            this.collection = new AlbumsCollection();
            var self = this;
            this.collection.fetch({
                success: function (albums) {
                    console.log(albums.length + " albums found");
                    var albumsView = new GalleryManager.AlbumsCollectionView({collection: self.collection});

                    GalleryManager.regions.main.show(albumsView);
                }
            });

            return false;
        }
    }
});

GalleryManager.module("GalleryApp.View", function (View, GalleryManager, Backbone, Marionette, $, _) {
    View.Controller = {
        viewAlbum: function (id, page) {
            this.collection = new ImagesCollection();
            this.collection.url = 'http://127.0.0.1:8000/app_dev.php/album/' + id + '/page/' + page;
            var self = this;
            this.collection.fetch({
                success: function (images) {
                    console.log(images.length + " images found");

                    var imagesView = new GalleryManager.ImagesCollectionView({collection: self.collection});
                    GalleryManager.regions.pics.show(imagesView);

                    // render pager now
                    self.viewPager(id, page);

                }
            });

            return false;
        },
        viewPager: function (id, page) {
            this.collection = new PagerLinksCollection();
            this.collection.url = 'http://127.0.0.1:8000/app_dev.php/album_pager/' + id + '/page/' + page;
            var self = this;
            this.collection.fetch({
                success: function (pages) {
                    console.log(pages.length + " pages found");

                    var pagesView = new GalleryManager.PagerLinksCollectionView({collection: self.collection});
                    GalleryManager.regions.pager.show(pagesView);
                }
            });

            return false;
        }
    }
});

GalleryManager.on("start", function () {
    if (Backbone.history) {
        Backbone.history.start();

        if (this.getCurrentRoute() === "") {
            console.log("albums:list");
            GalleryManager.trigger("albums:list");
        }
    }
});

