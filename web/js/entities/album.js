AlbumManager.module("Entities", function (Entities, AlbumManager, Backbone, Marionette, $, _) {
    Entities.Album = Backbone.Model.extend({
        urlRoot: "albums",

        defaults: {
            id: "",
            name: "",
            url: ""
        }
    });

    //Entities.configureStorage("AlbumManager.Entities.Album");

    Entities.AlbumCollection = Backbone.Collection.extend({
        url: "albums",
        model: Entities.Album,
        comparator: "id"
    });

    Entities.configureStorage("AlbumManager.Entities.AlbumCollection");

    var initializeAlbums = function () {
        var albums = new Entities.AlbumCollection([
            {id: 1, id: "Alice", name: "Arten", url: "555-0184"},
            {id: 2, id: "Bob", name: "Brigham", url: "555-0163"},
            {id: 3, id: "Charlie", name: "Campbell", url: "555-0129"}
        ]);
        albums.forEach(function (album) {
            album.save();
        });
        return albums.models;
    };

    var API = {
        getAlbumEntities: function () {
            var albums = new Entities.AlbumCollection();
            var defer = $.Deferred();
            albums.fetch({
                success: function (data) {
                    defer.resolve(data);
                }
            });
            var promise = defer.promise();
            $.when(promise).done(function (fetchedAlbums) {
                if (fetchedAlbums.length === 0) {
                    // if we don't have any albums yet, create some for convenience
                    var models = initializeAlbums();
                    albums.reset(models);
                }
            });
            return promise;
        },

        getAlbumEntity: function (albumId) {
            var album = new Entities.Album({id: albumId});
            var defer = $.Deferred();
            setTimeout(function () {
                album.fetch({
                    success: function (data) {
                        defer.resolve(data);
                    },
                    error: function (data) {
                        defer.resolve(undefined);
                    }
                });
            }, 2000);
            return defer.promise();
        }
    };

    AlbumManager.reqres.setHandler("album:entities", function () {
        return API.getAlbumEntities();
    });

    AlbumManager.reqres.setHandler("album:entity", function (id) {
        return API.getAlbumEntity(id);
    });
});
