GalleryManager.module("GallerysApp.List", function (List, GalleryManager, Backbone, Marionette, $, _) {
    List.Controller = {
        listAlbums: function () {

            this.collection = new AlbumsCollection();
            var self = this;
            this.collection.fetch({
                success: function (albums) {
                    console.log(albums.length + " albums found");
                    var albumsView = new GalleryManager.AlbumsCollectionView({collection: self.collection});

                    GalleryManager.regions.main.show(albumsView);
                }
            });

            return false;

            var loadingView = new GalleryManager.Common.Views.Loading();
            GalleryManager.regions.main.show(loadingView);

            var fetchingGallerys = GalleryManager.request("gallery:entities");

            var gallerysListLayout = new List.Layout();
            var gallerysListPanel = new List.Panel();

            $.when(fetchingGallerys).done(function (gallerys) {
                var filteredGallerys = GalleryManager.Entities.FilteredCollection({
                    collection: gallerys,
                    filterFunction: function (filterCriterion) {
                        var criterion = filterCriterion.toLowerCase();
                        return function (gallery) {
                            if (gallery.get("firstName").toLowerCase().indexOf(criterion) !== -1
                                || gallery.get("lastName").toLowerCase().indexOf(criterion) !== -1
                                || gallery.get("phoneNumber").toLowerCase().indexOf(criterion) !== -1) {
                                return gallery;
                            }
                        };
                    }
                });

                if (criterion) {
                    filteredGallerys.filter(criterion);
                    gallerysListPanel.once("show", function () {
                        gallerysListPanel.triggerMethod("set:filter:criterion", criterion);
                    });
                }

                var gallerysListView = new List.Gallerys({
                    collection: filteredGallerys
                });

                gallerysListPanel.on("gallerys:filter", function (filterCriterion) {
                    filteredGallerys.filter(filterCriterion);
                    GalleryManager.trigger("gallerys:filter", filterCriterion);
                });

                gallerysListLayout.on("show", function () {
                    gallerysListLayout.panelRegion.show(gallerysListPanel);
                    gallerysListLayout.gallerysRegion.show(gallerysListView);
                });

                gallerysListPanel.on("gallery:new", function () {
                    var newGallery = new GalleryManager.Entities.Gallery();

                    var view = new GalleryManager.GallerysApp.New.Gallery({
                        model: newGallery
                    });

                    view.on("form:submit", function (data) {
                        if (gallerys.length > 0) {
                            var highestId = gallerys.max(function (c) {
                                return c.id;
                            }).get("id");
                            data.id = highestId + 1;
                        }
                        else {
                            data.id = 1;
                        }
                        if (newGallery.save(data)) {
                            gallerys.add(newGallery);
                            view.trigger("dialog:close");
                            var newGalleryView = gallerysListView.children.findByModel(newGallery);
                            // check whether the new gallery view is displayed (it could be
                            // invisible due to the current filter criterion)
                            if (newGalleryView) {
                                newGalleryView.flash("success");
                            }
                        }
                        else {
                            view.triggerMethod("form:data:invalid", newGallery.validationError);
                        }
                    });

                    GalleryManager.regions.dialog.show(view);
                });

                gallerysListView.on("childview:gallery:show", function (childView, args) {
                    GalleryManager.trigger("gallery:show", args.model.get("id"));
                });

                gallerysListView.on("childview:gallery:edit", function (childView, args) {
                    var model = args.model;
                    var view = new GalleryManager.GallerysApp.Edit.Gallery({
                        model: model
                    });

                    view.on("form:submit", function (data) {
                        if (model.save(data)) {
                            childView.render();
                            view.trigger("dialog:close");
                            childView.flash("success");
                        }
                        else {
                            view.triggerMethod("form:data:invalid", model.validationError);
                        }
                    });

                    GalleryManager.regions.dialog.show(view);
                });

                gallerysListView.on("childview:gallery:delete", function (childView, args) {
                    args.model.destroy();
                });

                GalleryManager.regions.main.show(gallerysListLayout);
            });
        }
    }
});
