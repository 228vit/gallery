GalleryManager.module("GallerysApp", function(GallerysApp, GalleryManager, Backbone, Marionette, $, _){
    GallerysApp.Router = Marionette.AppRouter.extend({
        appRoutes: {
            "album/:id": "showGallery",
            "album/:id/edit": "editGallery"
        }
    });

    var API = {
        listAlbums: function(){
            GallerysApp.List.Controller.listAlbums();
            //GalleryManager.execute("set:active:header", "album");
        },

        showGallery: function(id){
            GallerysApp.Show.Controller.showGallery(id);
            GalleryManager.execute("set:active:header", "album");
        },

        editGallery: function(id){
            GallerysApp.Edit.Controller.editGallery(id);
            GalleryManager.execute("set:active:header", "album");
        }
    };

    GalleryManager.on("album:list", function(){
        GalleryManager.navigate("album");
        API.listGallerys();
    });

    GalleryManager.on("album:filter", function(criterion){
        if(criterion){
            GalleryManager.navigate("album/filter/criterion:" + criterion);
        }
        else{
            GalleryManager.navigate("album");
        }
    });

    GalleryManager.on("contact:show", function(id){
        GalleryManager.navigate("album/" + id);
        API.showGallery(id);
    });

    GalleryManager.on("contact:edit", function(id){
        GalleryManager.navigate("album/" + id + "/edit");
        API.editGallery(id);
    });

    GallerysApp.on("start", function(){
        new GallerysApp.Router({
            controller: API
        });
    });
});
