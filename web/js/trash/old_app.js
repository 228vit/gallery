(function ($) {

    // Create our Application
    var app = new Mn.Application();

    // Start history when our application is ready
    app.on('start', function () {
        Backbone.history.start();
    });

    app.addRegions({
        mainRegion: "#main-content",
        pagerRegion: "#pager"
    });

    app.on("before:start", function (options) {
        // remove any content in main container
        $("#main_content").html('');
    });

    var myController = {
        loadAlbum: function () {
            console.log('click on album');
        }
    };

    var MyRouter = new Marionette.AppRouter({
        controller: myController,
        appRoutes: {
            "album/:id": "loadAlbum"
        }
    });

    // global var
    pagerData = [];

    var Album = Backbone.Model.extend({
        defaults: {
            id: 1,
            name: "Album #1",
            url: "album/1"
        }
    });

    var AlbumView = Backbone.View.extend({
        tagName: "p",
        className: "albumContainer",
        template: $("#albumTemplate").html(),

        render: function () {
            var tmpl = _.template(this.template); //tmpl is a function that takes a JSON object and returns html

            this.$el.html(tmpl(this.model.toJSON())); //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            return this;
        }
    });

    var AlbumLibrary = Backbone.Collection.extend({
        model: Album
    });


    var AlbumLibraryView = Backbone.View.extend({

        el: $("#main_content"),

        initialize: function () {
            albumsData = [];
            albumImagesData = [];

            var that = this;

            $.ajax({
                url: '/',
                dataType: 'json'
            }).done(function (json) {
                // reset header
                $('#main_header').html('Albums list');
                // reset content
                $("#main_content").html('');

                albumsData = json;
                that.collection = new AlbumLibrary(albumsData);
                that.renderAlbumsList();
                reAssignClickEvents();
                // reset header
                $('#main_header').html('List of albums')
            });
        },

        renderAlbumsList: function () {
            var that = this;
            _.each(this.collection.models, function (item) {
                that.render(item);
            }, this);
        },

        render: function (item) {
            var albumView = new AlbumView({
                model: item
            });
            this.$el.append(albumView.render().el);

            $("#backButton").html('<a href="javascript:;" class="btn btn-success" id="back_to_list">« back to albums</a>');
            reAssignClickEvents();
        }
    });

    // render albums
    var view = new AlbumLibraryView();


    function reAssignClickEvents() {
        $('.link_to_album').click(function (event) {
            event.preventDefault();
            console.log($(this).attr('data-id'));

            // global var!
            album_id = $(this).attr('data-id');
            page = $(this).attr('data-page');

            // run render album with images
            var view = new AlbumImageLibraryView();

            return false;
        })

        $('#back_to_list').click(function (event) {
            // run render albums list
            event.preventDefault();
            var view = new AlbumLibraryView();

            return false;
        })
    }


    /*
     * Album images
     */
    var AlbumImage = Backbone.Model.extend({
        defaults: {
            id: 1,
            album_id: 1,
            title: "Album #1",
            thumb: "http://placehold.it/120x80",
            pic: "http://placehold.it/800x600",
        }
    });

    var AlbumImageView = Backbone.View.extend({
        //tagName: "div",
        //className: "albumImageContainer",
        template: $("#albumImageTemplate").html(),

        render: function () {
            var tmpl = _.template(this.template); //tmpl is a function that takes a JSON object and returns html

            this.$el.html(tmpl(this.model.toJSON())); //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            return this;
        }
    });

    var AlbumImageLibrary = Backbone.Collection.extend({
        model: AlbumImage
    });


    var AlbumImageLibraryView = Backbone.View.extend({

        el: $("#main_content"),

        initialize: function () {
            albumImagesData = [];
            var that = this;
            $.ajax({
                url: '/album/' + album_id + '?page=' + page,
                dataType: 'json',
            }).done(function (json) {
                albumImagesData = json;
                // reset header
                $('#main_header').html('View album ' + albumImagesData.album.name)
                // reset content
                $("#main_content").html('');

                that.collection = new AlbumImageLibrary(albumImagesData.images_paginator.items);
                that.render();

                reAssignClickEvents();

                pagerData = albumImagesData.pager;
                // render pager!
                var view = new AlbumPagerView();

            });
        },

        render: function () {
            var that = this;
            _.each(this.collection.models, function (item) {

                that.renderAlbumImage(item);
            }, this);
        },

        renderAlbumImage: function (item) {
            var albumImageView = new AlbumImageView({
                model: item
            });
            this.$el.append(albumImageView.render().el);
        }
    });

    /*
     * Album pager
     */
    var PagerItem = Backbone.Model.extend({
        defaults: {
            page: 1,
            album_id: 1,
            css_class: 'active'
        }
    });

    var PagerItemView = Backbone.View.extend({
        tagName: 'li',
        //className: "albumImageContainer",
        template: $("#albumPagerTemplate").html(),
        render: function () {
            var tmpl = _.template(this.template); //tmpl is a function that takes a JSON object and returns html

            this.$el.html(tmpl(this.model.toJSON())); //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            return this;
        }
    });

    var PagerItemLibrary = Backbone.Collection.extend({
        model: PagerItem
    });

    var AlbumPagerView = Backbone.View.extend({

        el: $("#albumPager"),

        initialize: function () {
            $('#albumPager').html('');

            console.log(pagerData);
            var that = this;
            that.collection = new PagerItemLibrary(pagerData); // from global var
            that.render();
        },

        render: function () {
            var that = this;
            _.each(this.collection.models, function (item) {
                console.log(item);

                that.renderPagerItem(item);
            }, this);

            console.log('render done');
            console.log($('#albumPager').html());
            $("#backButton").html('<a href="javascript:;" class="btn btn-success" id="back_to_list">« back to albums</a>');

            reAssignClickEvents();
        },

        renderPagerItem: function (item) {
            var pagerItemView = new PagerItemView({
                model: item
            });
            this.$el.append(pagerItemView.render().el);
        }
    });
})(jQuery);
