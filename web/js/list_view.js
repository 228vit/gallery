GalleryManager.module("GallerysApp.List", function(List, GalleryManager, Backbone, Marionette, $, _){
  List.Layout = Marionette.LayoutView.extend({
    template: "#gallery-list-layout",

    regions: {
      panelRegion: "#panel-region",
      gallerysRegion: "#gallerys-region"
    }
  });

  List.Panel = Marionette.ItemView.extend({
    template: "#gallery-list-panel",

    triggers: {
      "click button.js-new": "gallery:new"
    },

    events: {
      "submit #filter-form": "filterGallerys"
    },

    ui: {
      criterion: "input.js-filter-criterion"
    }
  });

  List.Gallery = Marionette.ItemView.extend({
    tagName: "tr",
    template: "#gallery-list-item",

    triggers: {
      "click td a.js-show": "gallery:show",
      "click td a.js-edit": "gallery:edit",
      "click button.js-delete": "gallery:delete"
    },

    events: {
      "click": "highlightName"
    },

    flash: function(cssClass){
      var $view = this.$el;
      $view.hide().toggleClass(cssClass).fadeIn(800, function(){
        setTimeout(function(){
          $view.toggleClass(cssClass)
        }, 500);
      });
    },

    highlightName: function(e){
      this.$el.toggleClass("warning");
    },

    remove: function(){
      var self = this;
      this.$el.fadeOut(function(){
        Marionette.ItemView.prototype.remove.call(self);
      });
    }
  });

  var NoGallerysView = Marionette.ItemView.extend({
    template: "#gallery-list-none",
    tagName: "tr",
    className: "alert"
  });

  List.Gallerys = Marionette.CompositeView.extend({
    tagName: "table",
    className: "table table-hover",
    template: "#gallery-list",
    emptyView: NoGallerysView,
    childView: List.Gallery,
    childViewContainer: "tbody",

    initialize: function(){
      this.listenTo(this.collection, "reset", function(){
        this.attachHtml = function(collectionView, childView, index){
          collectionView.$el.append(childView.el);
        }
      });
    },

    onRenderCollection: function(){
      this.attachHtml = function(collectionView, childView, index){
        collectionView.$el.prepend(childView.el);
      }
    }
  });
});
